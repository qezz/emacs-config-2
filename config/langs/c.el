(defun ccc-test ()
  (when (locate-library "smart-tabs-mode")
    (setq tab-width 2)
    (setq indent-tabs-mode nil))
  (setq show-trailing-whitespace t)
  (delete-trailing-whitespace-mode 'clean))

(add-hook 'c-mode-common-hook #'ccc-test)
