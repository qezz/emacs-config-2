(defun setup-auctex ()
  ;; Auctex
  (setq LaTeX-verbatim-environments ;; enable monospace formatting for this blocks
	'("verbatim" "verbatim*" "lstlisting" "minted" "codeframe" "rustcode" "ccode")))

(setup-auctex)
