(defun setup-rust ()
  "Setup Rust related variables"

  ;; (unless (package-installed-p 'rust-mode)
  ;;   (mapcar 'package-install '(rust-mode company-racer racer cargo)))

  ;; install needed packages
  (mapcar 'package-install
	  (seq-filter (lambda (p) (not (package-installed-p p)))
		      '(rust-mode company-racer racer)))
  (package-install 'cargo) ;; FIX: Warns when installing within script file

  (when on-mac
    (setenv "PATH" (concat "/usr/local/bin:" home-dir "/.cargo/bin:" "/usr/local/go/bin:" (getenv "PATH") ":/Library/TeX/texbin"))
    (setenv "RUST_SRC_PATH" "/Users/sergey/.rustup/toolchains/nightly-x86_64-apple-darwin/lib/rustlib/src/rust/src"))

  ;; TODO: Refine this mess
  (add-hook 'rust-mode-hook
	    '(lambda ()
	       (setq show-trailing-whitespace t)
	       (delete-trailing-whitespace-mode 'clean)
	       (setq rust-indent-method-chain t)
	       (setq indent-tabs-mode nil)
	       (racer-mode)
	       (cargo-minor-mode)))

  ;; (add-hook 'racer-mode-hook
  ;;           #'eldoc-mode)

  (add-hook 'racer-mode-hook
	    '(lambda ()
	       (company-mode)))
  (add-hook 'racer-mode-hook #'eldoc-mode)

  ;; (add-hook 'rust-mode-hook 'cargo-minor-mode)

  (require 'rust-mode)
  (define-key rust-mode-map (kbd "TAB") '(lambda () (interactive) (company-indent-or-complete-common)))
  (setq company-tooltip-align-annotations t)
  )

(setup-rust)
