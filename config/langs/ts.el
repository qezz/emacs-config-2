(add-hook 'typescript-mode-hook
	  '(lambda ()
	     (setq show-trailing-whitespace t)
	     (delete-trailing-whitespace-mode 'clean)
	     (setq indent-tabs-mode nil)))
