(defun setup-python ()
  (add-hook 'python-mode-hook
	    '(lambda ()
	       (define-key python-mode-map (kbd "<RET>") 'newline-and-indent)
	       (modify-syntax-entry ?_ "w")
	       (setq show-trailing-whitespace t)
	       (delete-trailing-whitespace-mode 'clean)
	       ))

  ;; use python3
  ;; (setf python-shell-interpreter "python")
  ;; (require 'python)


  ;; Hacks for python 3
  (when on-mac
    (defun python-shell-parse-command ()
      "Return the string used to execute the inferior Python process."
      "/usr/local/bin/python3 -i")
    (setq python-shell-interpreter "python3") ;; use particular python version
    (setq python-shell-prompt-detect-failure-warning nil) ;; suppress anoying warning about 'indent' detection
    )

  ;; Fix for "native completion" warning when using python3
  (defun python-shell-completion-native-try ()
    "Return non-nil if can trigger native completion"
    (with-eval-after-load 'python
      '(let ((python-shell-completion-native-enable t)
	     (python-shell-completion-native-output-timeout
	      python-shell-completion-native-try-output-timeout))
	 (python-shell-completion-native-get-completions
	  (get-buffer-process (current-buffer))
	  nil "_")))
    )

  (setenv "LC_CTYPE" "UTF-8")
  (setenv "LC_ALL" "en_US.UTF-8")
  (setenv "LANG" "en_US.UTF-8")

  )

(setup-python)
