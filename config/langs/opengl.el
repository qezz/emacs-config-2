(defun setup-opengl ()
  (mapcar 'package-install
	  (seq-filter (lambda (p) (not (package-installed-p p)))
		      '(glsl-mode company-glsl))))

(setup-opengl)
