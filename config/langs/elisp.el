(defun setup-emacs-lisp ()
  (add-hook 'emacs-lisp-mode-hook
	    '(lambda ()
	       (setq show-trailing-whitespace t)
	       (delete-trailing-whitespace-mode 'clean))))

(setup-emacs-lisp)
