;; (add-to-list 'load-path (concat config-home "go-mode.el/"))
;; (require 'go-mode-autoloads)
;;;;

(defun setup-go ()
  (setenv "GOPATH" (concat home-dir "/go-projects"))

  ;; (require 'company)                                   ; load company mode
  ;; (require 'company-go)

  ;; (setq company-tooltip-limit 20)                      ; bigger popup window
  ;; (setq company-idle-delay .3)                         ; decrease delay before autocompletion popup shows
  ;; (setq company-echo-delay 0)                          ; remove annoying blinking
  ;; (setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing

  ;; (add-hook 'go- #'gofmt-before-save)
  (add-hook 'go-mode-hook
	    '(lambda ()
	       (setq show-trailing-whitespace t)
               (delete-trailing-whitespace-mode 'clean)
	       (setq exec-path (append exec-path '("/usr/local/go/bin")))
	       (add-hook 'before-save-hook 'gofmt-before-save)
	       (set-variable 'tab-width 4)

	       ;; (set (make-local-variable 'company-backends) '(company-go))
	       ;; (company-mode)
	       (auto-complete-mode 1)

	       (let ((map go-mode-map))
		 (define-key map (kbd "C-c a") 'go-test-current-project) ;; current package, really
		 (define-key map (kbd "C-c t") 'go-test-current-file)
		 (define-key map (kbd "C-c .") 'go-test-current-test)
		 (define-key map (kbd "C-c r") 'go-run)
		 (define-key map (kbd "C-c b") 'go-build))

	       (local-set-key (kbd "M-.") 'godef-jump)
	       (local-set-key (kbd "M-,") 'pop-tag-mark)

	       ;; Customize compile command to run go build
	       ;; (if (not (string-match "go" compile-command))
	       (set (make-local-variable 'compile-command)
		    "go vet -v ./... && golint ./... && go build -v && go test -short -v ./...")))

  (with-eval-after-load 'go-mode
    (require 'go-autocomplete)
    (require 'auto-complete-config)
    (ac-config-default))

  ;; (require 'go-mode)
  ;; (define-key go-mode-map (kbd "TAB") '(lambda () (interactive) (company-indent-or-complete-common)))

  )

(setup-go)
