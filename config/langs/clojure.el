(require 'cider)

(setq cider-repl-use-pretty-print t)
(setq cider-repl-history-file (concat home-dir "/.emacs.d/cider-history"))

(eval-after-load 'flycheck '(flycheck-clojure-setup))
(add-hook 'after-init-hook #'global-flycheck-mode)

(with-eval-after-load 'flycheck
  (flycheck-pos-tip-mode))

(eval-after-load 'flycheck
  '(setq flycheck-display-errors-function #'flycheck-pos-tip-error-messages))

;; (with-eval-after-load 'flycheck
;;   '(add-hook 'flycheck-mode-hook 'flycheck-popup-tip-mode))

;; (eval-after-load 'flycheck
;;   '(setq flycheck-display-errors-function #'flycheck-popup-tip-show-popup))

;; (require 'flycheck-tip)
;; (flycheck-tip-use-timer 'verbose)

(add-hook 'clojure-mode-hook
	  '(lambda ()
	     (setq show-trailing-whitespace t)
	     (delete-trailing-whitespace-mode 'clean)
	     ))
