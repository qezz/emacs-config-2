(setq require-final-newline t) ;; add final new-line when saving

;; ac

(setq ac-auto-show-menu 0.1)

(when (locate-library "smart-tabs-mode")
  (require 'smart-tabs-mode)

  (smart-tabs-insinuate 'c
			;; 'c++
			'javascript))
