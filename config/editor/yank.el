;; Auto indent pasted code.
;; Btw, do not auto-indent python-code, it's probably already indented
;; for you; otherwise, you can't restore the context if it's not a
;; 2-lines-snippet
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode
				     lisp-mode       go-mode
				     clojure-mode    scheme-mode
				     haskell-mode    ruby-mode
				     rspec-mode      rust-mode cargo-mode
				     c-mode
				     ;; c++-mode
				     objc-mode       latex-mode
				     js              javascript
                                     clojure-mode clojurescript-mode
				     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))
