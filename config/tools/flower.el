(require 'cl)
(require 'flower)

;; Each element of `flower-tracker-queries` vector has the following format:
;; * Tracker URL or nil if only query changed
;; * Use tracker without auth if nil or with default auth otherwise - see Flower auth
;; * Tracker query or nil for default query if applicable
(setq flower-tracker-queries [("https://github.com/PositiveTechnologies/flower" nil nil)
                              ("https://github.com/melpa/melpa" nil nil)])

(add-hook 'org-mode 'flower-mode)

(global-set-key (kbd "C-c f f") 'flower-list-tasks)
(global-set-key (kbd "C-c f t") 'flower-show-task-info)
