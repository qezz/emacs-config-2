(defun setup-recentf-mode ()
  (init-open-recentf) ;; open file if provided as arg, open recentf otherwise
  (recentf-mode 1)
  (setq recentf-max-menu-items 25)
  (global-set-key "\C-x\ \C-r" 'recentf-open-files))

(defun setup-custom-scroll ()
  "Slow down the enormous scrilling speed"
  ;; (setq scroll-conservatively 1000000)
  ;; (setq scroll-preserve-screen-position 1)
  ;; (setq scroll-step 1)
  (setq mouse-wheel-scroll-amount '(4 ((shift) . 4))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 1) ;; keyboard scroll one line at a time
  )

(defun force-utf8 ()
  "Helpful when your system language encoding is not en_US.UTF-8"
  (setenv "LC_CTYPE" "UTF-8")
  (setenv "LC_ALL" "en_US.UTF-8")
  (setenv "LANG" "en_US.UTF-8"))

;; Disable warning for C-x C-l (downcase-region)
(put 'downcase-region 'disabled nil)
(fset 'yes-or-no-p 'y-or-n-p) ;; Use just 'y' or 'n' instead of 'yes' and 'no'
