(defvar GLOBAL-THEME 'spacemacs-light)

;; TODO: Add packages:
;; latex-mode, auctex, go-mode, gotfmt?, kotlin-mode

(package-initialize)
(unless (package-installed-p 'package)
  (package-install 'package))
(require 'package) ;; You might already have this line
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

;; update info about packages if it's not already
(when (not package-archive-contents)
    (package-refresh-contents))

;;;;

(defvar on-mac (eq window-system 'ns))
(defvar is-remacs (cl-search "Remacs" (emacs-version)))
(defvar do-setup-crossplatform-variables t)
(defvar home-dir (getenv "HOME"))
(defvar config-home (concat (or home-dir "~/") ;; `or' will get the first non-nil element
			    "/.emacs.d/"))

(savehist-mode 1)
(setq savehist-additional-variables
      '(compile-command kill-ring search-ring regexp-search-ring))

(when on-mac
  ;; (set-input-method 'russian-computer)
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-env "GOPATH"))

;; A hack to make new frames use this font
(set-frame-font "Fira Code" t t)

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file config-home)))

;; load ui first to decrease startup delay
(load-user-file "config/ui/global.el")
(load-user-file "config/ui/theme.el")

(load-user-file "config/ux/config.el")

(load-user-file "config/editor/yank.el")
(load-user-file "config/editor/keys.el")
(load-user-file "config/editor/global.el")

(load-user-file "config/langs/common.el")
(load-user-file "config/langs/rust.el")
(load-user-file "config/langs/go.el")
(load-user-file "config/langs/opengl.el")
(load-user-file "config/langs/python.el")
(load-user-file "config/langs/elisp.el")
(load-user-file "config/langs/auctex.el")
(load-user-file "config/langs/clojure.el")
(load-user-file "config/langs/js.el")
(load-user-file "config/langs/ts.el")
(load-user-file "config/langs/groovy.el")

(load-user-file "config/tools/flower.el")


;; load side things
(when (and (file-exists-p (concat config-home "site-start.el")) (not (symbol-function 'update-all-autoloads)))
  (load (concat config-home "site-start.el")))

(when do-setup-crossplatform-variables
  (force-utf8)
  (delete-selection-mode t) ;; wipe selected text while beginning writing smth; see https://www.emacswiki.org/emacs/DeleteSelectionMode for details

  (setq make-backup-files nil) ;; do not create things like `file~`

  ;; I do prefer wombat theme in terminal since it is better looking
  ;; than others
  (if window-system
      (load-theme GLOBAL-THEME t)
    (load-theme 'wombat))

  (setq nord-comment-brightness 20)

  (setup-custom-keys)

  (setq column-number-mode t) ;; show column above the minibuffer
  (setq inhibit-startup-screen t) ;; hide startup screen, so opens *scratch*
  (setq initial-scratch-message nil) ;; hide *scratch* intro message

  ;; (setq initial-buffer-choice 'recentf-open-files)
  (setup-recentf-mode)

  (setup-custom-scroll) ;; use linear scroll instead of progressive one

  (show-paren-mode 1) ;; highlight an expression in parens
  (setq show-paren-style (quote expression))

  (set-default 'truncate-lines t) ;; do truncate lines
  (setq truncate-partial-width-windows nil)

  (setq visible-bell t) ;; show the bell

  (setf completion-ignored-extensions ;; do not suggest files with these extensions
	(append completion-ignored-extensions
		'(".hi" ".pdf" ".o")))

  (when (locate-library "delete-trailing-whitespace-mode") ;; delete trailing whitespaces before saving
    (require 'delete-trailing-whitespace-mode)))

;; Workgroups
;; (require 'workgroups2)
;; ;; Change some settings
;; (setq wg-session-file "~/.emacs.d/.workgroups")
;; (workgroups-mode 1)        ; put this one at the bottom of .emacs (init.el)

;; ;; NeoTree
;; (require 'neotree)
;; (global-set-key [f8] 'neotree-toggle)
;; (setq neo-theme (if (display-graphic-p) 'arrow))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-command-list
   (quote
    (("xelatex shell-escaped" "xelatex --shell-escape -8bit %t" TeX-run-TeX nil t)
     ("xelatex shell-escaped-no-tabs" "xelatex --shell-escape %t" TeX-run-TeX nil t)
     ("TeX" "%(PDF)%(tex) %(file-line-error) %(extraopts) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("Makeinfo" "makeinfo %(extraopts) %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo %(extraopts) --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "amstex %(PDFout) %(extraopts) %`%S%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "%(cntxcom) --once --texutil %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "%(cntxcom) %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX")
     ("Biber" "biber %s" TeX-run-Biber nil t :help "Run Biber")
     ("View" "%V" TeX-run-discard-or-function t t :help "Run Viewer")
     ("Print" "%p" TeX-run-command t t :help "Print the file")
     ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     ("File" "%(o?)dvips %d -o %f " TeX-run-dvips t t :help "Generate PostScript file")
     ("Dvips" "%(o?)dvips %d -o %f " TeX-run-dvips nil t :help "Convert DVI file to PostScript")
     ("Dvipdfmx" "dvipdfmx %d" TeX-run-dvipdfmx nil t :help "Convert DVI file to PDF with dvipdfmx")
     ("Ps2pdf" "ps2pdf %f" TeX-run-ps2pdf nil t :help "Convert PostScript file to PDF")
     ("Glossaries" "makeglossaries %s" TeX-run-command nil t :help "Run makeglossaries to create glossary file")
     ("Index" "makeindex %s" TeX-run-index nil t :help "Run makeindex to create index file")
     ("upMendex" "upmendex %s" TeX-run-index t t :help "Run upmendex to create index file")
     ("Xindy" "texindy %s" TeX-run-command nil t :help "Run xindy to create index file")
     ("Check" "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     ("ChkTeX" "chktex -v6 %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for common mistakes")
     ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     ("Other" "" TeX-run-command t t :help "Run an arbitrary command"))))
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#d2ceda" "#f2241f" "#67b11d" "#b1951d" "#3a81c3" "#a31db1" "#21b8c7" "#655370"])
 '(c-basic-offset 4)
 '(c-default-style
   (quote
    ((java-mode . "java")
     (awk-mode . "awk")
     (other . "bsd"))))
 '(c-offsets-alist
   (quote
    ((arglist-intro . ++)
     (arglist-cont . 0)
     (arglist-cont-nonempty . ++)
     (inher-intro . ++)
     (member-init-cont . 0)
     (statement-cont . ++)
     (substatement-open . 0))))
 '(c-require-final-newline
   (quote
    ((c-mode . t)
     (objc-mode . t)
     (java-mode . t)
     (erlang-mode . t)
     (haskell-mode . t)
     (html-mode . t))))
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("73dff3837af32cadc13c3cbbfc2347ace9d944cf3d0d84c3dffaada7da911df4" "732bd21f6fec7f411f9542f0eea3c4ad1123429ff558cf9c17bf8c355e844d91" "adff0e99fa81ebfe4ad94e147098577b57b1aec0c4206960b8389c0cc9f1b235" "225b57dd6df9b8e0f46148f9dde3814a0c3da4f4a1afc2b2d7345d03406360e8" "4200a00085a4e5b2be3a7b93d9fd8a319e69e0c815556228dc3e1b1aa7ea1273" "4f2375c1d63d0a6891827497a9934520ed8945c52ffaaf328f9bd1ca8d2af8a6" "1e9a82d81024b614cbca0a061157b4ccd67ab92b88708c37f1fbd828f4f09815" "dc79be6b953656a5dfa9becd1b11bbbd238843e73be2daae798b11fdd30a9b57" "cb7136fb0edca59311608a040a91205fa2a42366489007b38c0ae009c7006607" "999a48a69cc2623350ad7181e574b453272e087aeeed5f0a1ea7e0fddf96c939" "bf390ecb203806cbe351b966a88fc3036f3ff68cd2547db6ee3676e87327b311" "8b159d400a0bdd52348bc2b03a53edf1ac7cc346319275df3df4fea87da67812" "17f5d853fcc0759ffdcf62cf2f934b47bc90e4e33e213154bc0cac5b28d7b5b8" "27690190f6b94fe5b2d60a97e7389c8f4d96b80f71859127b11129426d949ef8" "732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "9240e71034689655a6c05c04063af2c90d0a831aa4e7ca24c8b6e29b5a2da946" "c856158cc996d52e2f48190b02f6b6f26b7a9abd5fea0c6ffca6740a1003b333" "7d2e7a9a7944fbde74be3e133fc607f59fdbbab798d13bd7a05e38d35ce0db8d" "ef98b560dcbd6af86fbe7fd15d56454f3e6046a3a0abd25314cfaaefd3744a9e" "daa75c4b9e7c842bdc60b7f30822f8d309fdca089e1ffcda1123e12b8dfa3423" "e1006c03f5bdea06db1f058a6880c3c6bda25a7970e2625780301d0e8abb45d3" "1a079058ee494c52788d8b7790f614630f70d51eaf2a768258a73935690196fb" "7cb89c93e37327cf1545fd8579816241446c611ebabc144a9835d09bbe132b19" "eea01f540a0f3bc7c755410ea146943688c4e29bea74a29568635670ab22f9bc" "c3d4af771cbe0501d5a865656802788a9a0ff9cf10a7df704ec8b8ef69017c68" "62c81ae32320ceff5228edceeaa6895c029cc8f43c8c98a023f91b5b339d633f" "f27c3fcfb19bf38892bc6e72d0046af7a1ded81f54435f9d4d09b3bff9c52fc1" "5f4e4c9f5de8156f964fdf8a1b8f8f659efbfeff88b38f49ce13953a84272b77" "d79ece4768dfc4bab488475b85c2a8748dcdc3690e11a922f6be5e526a20b485" "47744f6c8133824bdd104acc4280dbed4b34b85faa05ac2600f716b0226fb3f6" "b9b1a8d2ec1d5c17700e1a09256f33c2520b26f49980ed9e217e444c381279a9" "cd4d1a0656fee24dc062b997f54d6f9b7da8f6dc8053ac858f15820f9a04a679" "7527f3308a83721f9b6d50a36698baaedc79ded9f6d5bd4e9a28a22ab13b3cb1" "ff79b206ad804c41a37b7b782aca44201edfa8141268a6cdf60b1c0916343bd4" "2dd51a71783f5b3aa6570e53d526350c8fe690ee7d13c26169a13a2e72436436" "e11569fd7e31321a33358ee4b232c2d3cf05caccd90f896e1df6cab228191109" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" default)))
 '(fci-rule-color "#383838")
 '(highlight-changes-colors (quote ("#FD5FF0" "#AE81FF")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#fdf6e3" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#586e75")
 '(highlight-tail-colors
   (quote
    (("#3C3D37" . 0)
     ("#679A01" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#3C3D37" . 100))))
 '(hl-bg-colors
   (quote
    ("#DEB542" "#F2804F" "#FF6E64" "#F771AC" "#9EA0E5" "#69B7F0" "#69CABF" "#B4C342")))
 '(hl-fg-colors
   (quote
    ("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3")))
 '(hl-paren-colors (quote ("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900")))
 '(hl-sexp-background-color "#1c1f26")
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-selected-packages
   (quote
    (flower flycheck-rust flycheck-popup-tip flycheck-tip flycheck-pos-tip flycheck-clojure cider-eval-sexp-fu elm-mode lsp-mode crappy-jsp-mode auctex magithub dante tickscript-mode vue-html-mode vue-mode ensime scala-mode typescript-mode go-autocomplete yaml-mode material-theme markdown-mode solarized-theme cider groovy-mode indium ansible nordless-theme gruvbox-theme suscolors-theme nord-theme darkburn-theme darkmine-theme darkokai-theme monokai-alt-theme monokai-theme zerodark-theme smooth-scroll smooth-scrolling zenburn-theme doneburn-theme neotree all-the-icons-dired workgroups2 magit color-theme spacemacs-theme init-open-recentf exec-path-from-shell company-go gotest python cargo smart-tabs-mode go-mode kotlin-mode docker-compose-mode dockerfile-mode mkdown markdown-preview-mode markdown-preview-eww markdown-mode+ gh-md glsl-mode reverse-im racer company-racer rust-mode)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(pos-tip-background-color "#FFFACE")
 '(pos-tip-foreground-color "#272822")
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(spacemacs-theme-underline-parens nil)
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(tramp-syntax (quote default) nil (tramp))
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3")
 '(weechat-color-list
   (quote
    (unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0")))
 '(xterm-color-names
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#073642"])
 '(xterm-color-names-bright
   ["#fdf6e3" "#cb4b16" "#93a1a1" "#839496" "#657b83" "#6c71c4" "#586e75" "#002b36"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(put 'upcase-region 'disabled nil)
