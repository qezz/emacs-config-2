(define-package "flower" "20190215.1746" "Emacs task tracker client."
  '((emacs "24.4")
    (clomacs "0.0.3"))
  :keywords
  '("hypermedia" "outlines" "tools" "vc")
  :authors
  '(("Sergey Sobko" . "SSobko@ptsecurity.com"))
  :maintainer
  '("Sergey Sobko" . "SSobko@ptsecurity.com")
  :url "https://github.com/PositiveTechnologies/flower")
;; Local Variables:
;; no-byte-compile: t
;; End:
