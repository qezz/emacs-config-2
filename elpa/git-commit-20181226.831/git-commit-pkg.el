;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20181226.831" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (with-editor "20181103")) :commit "7f2a45c12be5400d974aea62864085b3d2981940" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
