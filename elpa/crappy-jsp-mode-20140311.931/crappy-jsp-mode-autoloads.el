;;; crappy-jsp-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "crappy-jsp-mode" "crappy-jsp-mode.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from crappy-jsp-mode.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "crappy-jsp-mode" '("cjsp-" "crappy-jsp-mode" "jsp-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; crappy-jsp-mode-autoloads.el ends here
